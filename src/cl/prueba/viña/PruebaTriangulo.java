package cl.pruba.vi�a;

import static org.junit.Assert.*;

import org.junit.Test;

public class PruebTriangulo {

	@Test
	public void pruebaIsosceles1() {
		//llamamos a la clase triangulo y le asignamos valores:
		Triangulo triangulo = new Triangulo(10,10,19);
		//creamos metodo especial para comparar
		assertEquals("isosceles", triangulo.tipoDeTriangulo());
				
	}

	@Test
	public void pruebaEquilatero() {
		Triangulo triangulo = new Triangulo(5,5,5);
		assertEquals("equilatero", triangulo.tipoDeTriangulo());
				
	}
	
	@Test
	public void pruebaEscaleno() {
		Triangulo triangulo = new Triangulo(1,2,3);
		assertEquals("escaleno", triangulo.tipoDeTriangulo());
	}
	
	@Test
	public void pruebaTriangulo1() {
		Triangulo triangulo = new Triangulo(10,10,21);
		assertEquals("NO es triangulo", triangulo.esTriangulo());
	}
	
	@Test
	public void pruebaTriangulo2() {
		Triangulo triangulo = new Triangulo(10,10,15);
		assertEquals("SI es triangulo", triangulo.esTriangulo());
	}
	
	
	//creamos pruebas de fallas:
	
	
	@Test
	public void pruebaEscalenoFallada() {
		Triangulo triangulo = new Triangulo(2,2,3);
		assertEquals("escaleno", triangulo.tipoDeTriangulo());
	}
	
	@Test
	public void pruebaEquilateroFallada() {
		Triangulo triangulo = new Triangulo(5,5,10);
		assertEquals("equilatero", triangulo.tipoDeTriangulo());
				
	}
	
	//creamos pruebas de error: 
	
	@Test
	public void testEsTrianguloError() {			
		Triangulo triangulo = new Triangulo("b",20,30);
		assertEquals("NO es triangulo", triangulo.esTriangulo());	
	}
	
	@Test
	public void pruebaIsoscelesError() {
		Triangulo triangulo = new Triangulo(19,"b",10);
		assertEquals("isosceles", triangulo.tipoDeTriangulo());
				
	}

}
