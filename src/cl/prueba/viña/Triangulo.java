package cl.pruba.vi�a;

//creo las clases para los lados (privadas)
public class Triangulo {
		private int ladoA;
		private int ladoB;
		private int ladoC;
			
		//generamos c�digo autogenerado a trav�s de un constructor vacio sin parametros
		public Triangulo() {
			super();
		}
		
		//creo un segundo constructor donde podemos ingresar directamente todos los parametros
		public Triangulo(int ladoA, int ladoB, int ladoC) {
			super();
			this.ladoA = ladoA;
			this.ladoB = ladoB;
			this.ladoC = ladoC;
		}
						
		public String esTriangulo(){
			if(ladoA+ladoB>ladoC && ladoB+ladoC>ladoA && ladoA+ladoC>ladoB){
				return "SI es triangulo";
			}
			else{
				return "NO es triangulo";
			}
		}
		
		public String tipoDeTriangulo() {
			if(ladoA == ladoB && ladoA == ladoC) {
				return "equilatero";
			}
			else if(ladoA != ladoB && ladoB != ladoC && ladoC != ladoA) {
				return "escaleno";
			}
			else {
				return "isosceles";
			}
		}

	}